#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb@libreapps.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import os
from uuid import uuid4
import re
import csv

dir = os.path.join("gmptedata")
for f in os.listdir(dir):
    m = re.search("_([A-Z0-9]+)_", f)
    print f
    service = m.group(1)
    startdate = -1
    enddate = -1
    journeytext = ""
    tmpstops = {}
    journeystops = {}
    journeystopsitems = {}
    file = open(os.path.join(dir,f))
    for line in file:
        code = line[:2]
        if code == "ZA":
            stopid = line[3:14]
            name = line[15:63]
            tmpstops[stopid] = ([stopid,name,""])
        elif code == "ZS":
            journeytext = line[14:63].strip()
        elif code == "QS":
            days = []
            for i in range(30,37):
                days.append(line[i-1:i])
            startdate = int(line[13:21])
            enddate = int(line[21:29])
        elif code == "QI" or code == "QO" or code == "QT":
            today = 20100906
            if (startdate <= today and (enddate > today or enddate == 0)):
                if not journeytext in journeystops:
                    journeystops[journeytext] = {}
                    journeystopsitems[journeytext] = []
                if not stopid in journeystops[journeytext]:
                    journeystops[journeytext][stopid] = tmpstops[stopid]
                    journeystopsitems[journeytext].append((stopid, tmpstops[stopid]))
                stopid = line[2:13]
                time = line[14:18]
                tmpf = open(os.path.join("stops",stopid+".csv"), "a")
                stopout = csv.writer(tmpf)
                stopout.writerow([service,time] + days)
                tmpf.close()
    for journeytext, stopsitems in journeystopsitems.items():
        print journeytext
        if not os.path.exists(os.path.join("routes",service)):
            os.makedirs(os.path.join("routes",service))
        fout = open(os.path.join("routes",service,journeytext+".csv"), "w")
        out = csv.writer(fout)
        print stopsitems
        for id, stop in stopsitems:
            out.writerow(stop)
    fout.close()
