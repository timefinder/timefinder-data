#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb@libreapps.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import os
from uuid import uuid4
import re
import csv
import xml.etree.ElementTree as ET
import datetime
import dateutil.parser

TE = "{http://www.transxchange.org.uk/}"
dir = "tfl-timetable-listing"

timere = re.compile("PT([0-9]+)S")

dir = os.path.join(dir)
for f in os.listdir(dir):
    print f
    xml = open(os.path.join(dir, f))
    tree = ET.parse(xml)
    root = tree.getroot()
    
    # Temporary dict for storing stop names
    tmpstops = {}
    servicecode = ""
    jp_dict = {}
    jps_timedeltas = {}
    routenames = {}
    daydict = {
        "MondayToFriday": [1,1,1,1,1,0,0],
        "Saturday": [0,0,0,0,0,1,0],
        "Sunday": [0,0,0,0,0,0,1],
        "Monday": [1,0,0,0,0,0,0],
        "Tuesday": [0,1,0,0,0,0,0],
        "Wednesday": [0,0,1,0,0,0,0],
        "Thursday": [0,0,0,1,0,0,0],
        "Friday": [0,0,0,0,1,0,0]
    }
    
    #for t in root:
    #    print t
    
    stoppoints = root.find(TE+"StopPoints")
    for stoppoint in stoppoints:
        stoppointref = stoppoint.find(TE+"StopPointRef").text
        commonname = stoppoint.find(TE+"CommonName").text
        tmpstops[stoppointref] = commonname
    
    routes = root.find(TE+"Routes")
    for route in routes:
        routenames[route.find(TE+"RouteSectionRef").text] = route.find(TE+"Description").text
    
    services = root.find(TE+"Services")
    for service in services:
        #print ET.tostring(service)
        servicecode = service.find(TE+"Lines").find(TE+"Line").find(TE+"LineName").text
        standardservice = service.find(TE+"StandardService")
        #print ET.tostring(standardservice)
        for jp in standardservice.findall(TE+"JourneyPattern"):
            id = jp.get("id")
            jp_dict[id] = []
            for jpsr in jp.findall(TE+"JourneyPatternSectionRefs"):
                jp_dict[id].append( jpsr.text )
                break # FIXME
    
    routesections = root.find(TE+"RouteSections")
    doneids = []
    for routesection in routesections:
        try:
            routesectionref = routesection.get("id")
            journeytext = routenames[routesectionref]
            journeytext = journeytext.replace("/", "-")
            if not os.path.exists(os.path.join("routes",servicecode)):
                os.makedirs(os.path.join("routes",servicecode))
            
            print journeytext
            fout = open(os.path.join("routes",servicecode,journeytext+".csv"), "w")
            out = csv.writer(fout)
            for routelink in routesection:
                id1 = routelink.find(TE+"From").find(TE+"StopPointRef").text
                id2 = routelink.find(TE+"To").find(TE+"StopPointRef").text
                for id in id1, id2:
                    if not id in doneids:
                        doneids.append(id)
                        out.writerow([id,tmpstops[id]])
            fout.close()
        except KeyError:
            print "RouteSections KeyError"
        
            
    
    jpss = root.find(TE+"JourneyPatternSections")
    for jps in jpss:
        id = jps.get("id")
        timedeltas = []
        i=0
        for jptl in jps:
            if i == 1:
                timedeltas.append((jptl.find(TE+"From").find(TE+"StopPointRef").text, datetime.timedelta(seconds=0)))
            stoppointref = jptl.find(TE+"To").find(TE+"StopPointRef").text
            timecode = jptl.find(TE+"RunTime").text
            m = timere.match(timecode)
            td = datetime.timedelta(seconds=int(m.group(1)))
            timedeltas.append((stoppointref, td))
            i+=1
        jps_timedeltas[id] = timedeltas
    
    #print jps_timedeltas
    
    vehiclejourneys = root.find(TE+"VehicleJourneys")
    i=0
    for journey in vehiclejourneys:
        id = journey.find(TE+"JourneyPatternRef").text
        for day in journey.find(TE+"OperatingProfile").find(TE+"RegularDayType").find(TE+"DaysOfWeek"):
            dayname = day.tag.replace(TE, "")
            break # FIXME
        start = dateutil.parser.parse( journey.find(TE+"DepartureTime").text )
        curr = start
        for jpid in jp_dict[id]:
            for stopid, dt in jps_timedeltas[jpid]:
                curr += dt
                time = curr.strftime("%H%M")
                
                tmpf = open(os.path.join("stops",stopid+".csv"), "a")
                stopout = csv.writer(tmpf)
                days = daydict[dayname]
                stopout.writerow([servicecode,time] + days)
                tmpf.close()
        i+=1
    print i
    print
    
    
    #break
